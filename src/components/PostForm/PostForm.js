import React from 'react';
import './PostForm.css';

const PostForm = props => {
    return (
        <form action="#">
            <legend>{props.legend}</legend>

            <label htmlFor="title">Title
                <input type="text" id="title"
                       name="title"
                       value={props.title}
                       onChange={props.changeValue}
                />
            </label>

            <label htmlFor="body">Description
                <textarea name="body" id="body"
                          cols="30" rows="10"
                          value={props.body}
                          onChange={props.changeValue}
                />
            </label>

            <button onClick={props.createPost}>Save</button>
        </form>
    )
};

export default PostForm;