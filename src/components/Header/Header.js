import React from 'react';
import {NavLink} from 'react-router-dom';
import './Header.css';

const Header = () => {
    return (
        <header>
            <h2>
                <NavLink to="/">My Blog</NavLink>
            </h2>

            <ul>
                <li>
                    <NavLink to="/posts">Home</NavLink>
                </li>
                <li>
                    <NavLink to="/posts/add">Add</NavLink>
                </li>
            </ul>
        </header>
    )
};

export default Header;